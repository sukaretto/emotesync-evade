local game = game;
local httpGet = game.HttpGet;
local function getRepo(author, repo, fileName, branch)
    return loadstring(httpGet(game, ('https://raw.githubusercontent.com/%s/%s/%s/%s'):format(author, repo, branch or 'master', fileName)))();
end;

local quick = getRepo('Belkworks', 'quick', 'init.lua');
local broom = getRepo('Belkworks', 'broom', 'init.lua');

-- services.
local s = quick.Service;
local workspace = s.Workspace;
local replicatedStorage = s.ReplicatedStorage;
local players = s.Players;
local stats = s.Stats;

-- vm optimization.
local taskWait, taskDelay = task.wait, task.delay;
local mathAbs, stringGsub = math.abs, string.gsub;

local getChildren, getAttribute = game.GetChildren, game.GetAttribute;
local findFirstChild, waitForChild = game.FindFirstChild, game.WaitForChild;

local create, destroy = Instance.new, game.Destroy;

local fireServer, invokeServer = create'RemoteEvent'.FireServer, create'RemoteFunction'.InvokeServer;
local getPlayingAnimations = create'Animator'.GetPlayingAnimationTracks;

local dataPing = stats.Network.ServerStatsItem['Data Ping'];
local getValue = dataPing.GetValue;

local connect, resolve;
do
    local _ = game.Loaded;
    connect, resolve = _.Connect, _.Wait;
end;

-- client & character.
local client = quick.User;
local character = client.Character or resolve(client.CharacterAdded);
local humanoidRootPart = waitForChild(character, 'HumanoidRootPart');

--- funcs.
local getEmotes = function() local _ = {}; for i = 1, 6 do _[getAttribute(client, 'Emote' .. i)] = i end; return _ end;

local getPlayingEmote;
do
    local emoteIdMap = {}
    for _, emoteModule in getChildren(replicatedStorage.Items.Emotes) do
        if (emoteModule.Name == 'Test') then continue end;
        emoteIdMap[emoteModule.Animation.AnimationId] = emoteModule.Name;
    end;

    getPlayingEmote = function(humanoidAnimator)
        local emoteInfo = {}
        for _, track in getPlayingAnimations(humanoidAnimator) do
            local trackName = emoteIdMap[track.Animation.AnimationId];
            if (not trackName) then continue end;

            emoteInfo = { Name = trackName, Animation = track }
            break;
        end;

        return emoteInfo;
    end;
end;

local playEmote;
do
    local emoteEvent, emoteEquip;
    do
        local _ = replicatedStorage.Events;
        local __ = _.UI.Equip;
        emoteEvent, emoteEquip = _.Emote, function(emoteName) return invokeServer(__, 'Emotes', emoteName, 'Emote1') end;
    end;

    playEmote = function(playerEmote, playerCharacter)
        local emoteSlot, emoteTime = getEmotes()[playerEmote.Name], playerEmote.Animation.Length - playerEmote.Animation.TimePosition;
        if (not emoteSlot) then
            local emoteOriginal = getAttribute(client, 'Emote1');
            if (not emoteEquip(playerEmote.Name)) then
                return;
            end;

            emoteSlot = 1;
            taskDelay(emoteTime + 1, emoteEquip, emoteOriginal);
        end;

        taskWait(mathAbs(emoteTime - (getValue(dataPing) * 0.00135)));
        if (humanoidRootPart.Position - playerCharacter.HumanoidRootPart.Position).Magnitude < 12 and
            getAttribute(playerCharacter, 'Emoting') then fireServer(emoteEvent, tostring(emoteSlot)) end;
    end;
end;

local function getPlayerCharacterFromModel(i)
    i = i.ClassName == 'Model' and i;

    local player = i and findFirstChild(players, i.Name);
    return (player and player ~= client) and (findFirstChild(i, 'HumanoidRootPart') and findFirstChild(i, 'Humanoid')) ~= nil and i;
end;

-- (●'◡'●)
local emoteHitbox = quick.Instance(create 'Part', { CanCollide = false, Massless = true, Transparency = 1, Size = Vector3.new(12, 9, 12), Parent = workspace });
local hitboxWeld = quick.Instance(create 'Weld', { Part0 = humanoidRootPart, Part1 = emoteHitbox, Parent = emoteHitbox });

do
    local promptBroom = broom();
    connect(emoteHitbox.Touched, function(i)
        local playerCharacter = character and not (getAttribute(character, 'Emoting') or getAttribute(character, 'Downed')) and (i.Parent and getPlayerCharacterFromModel(i.Parent));
        local playerEmote = playerCharacter and not findFirstChild(playerCharacter.HumanoidRootPart, 'EmotePrompt') and (function()
            local _ = getPlayingEmote(playerCharacter.Humanoid.Animator);
            return (_.Animation and getAttribute(client.Emotes, _.Name)) and _;
        end)();

        if (not playerEmote) then return end;
        local emotePrompt = quick.Instance(create 'ProximityPrompt', {
            Name = 'EmotePrompt',
            ObjectText = stringGsub(playerEmote.Name, '(%l)(%u)', '%1 %2'),
            ActionText = 'Sync Emote',
            HoldDuration = 0.3,
            RequiresLineOfSight = false,
            Parent = playerCharacter.HumanoidRootPart
        }); do
            promptBroom.__instance = emotePrompt;

            for _, v in { playerEmote.Animation.Stopped, playerCharacter.Humanoid.Died } do promptBroom:connectClean(v) end;
            promptBroom:give(connect(emotePrompt.Triggered, function()
                promptBroom:clean();
                playEmote(playerEmote, playerCharacter);
            end));
        end;
    end);

    connect(emoteHitbox.TouchEnded, function(i)
        i = i.Parent and getPlayerCharacterFromModel(i.Parent);

        local emotePrompt = i and findFirstChild(i.HumanoidRootPart, 'EmotePrompt');
        if (emotePrompt) then destroy(emotePrompt) end;
    end);
end;

connect(client.CharacterAdded, function(newCharacter)
    character, humanoidRootPart = newCharacter, waitForChild(newCharacter, 'HumanoidRootPart');
    hitboxWeld.Part0 = humanoidRootPart;
end);
